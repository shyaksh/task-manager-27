#!/bin/bash

echo "SHUTDOWN TASK MANAGER SERVER...";

if [ ! -f ./tm-server.pid ]; then
  echo "tm-server.pid not found"
  exit 1;
fi

#kill -9 $(pgrep -f task-manager-server.jar)
echo "KILL PROCESS WITH PID "$(cat ./tm-server.pid);
kill -9 $(cat ./tm-server.pid)
rm ./tm-server.pid
echo "OK";
