#!/bin/bash

echo "STARTING TASK MANAGER SERVER..."

if [ -f ./tm-server.pid ]; then
  echo "Application already started!"
  exit 1;
fi

mkdir -p ../log
rm -f ../log/tm-server.log
nohup java -jar ./task-manager-server.jar > ../log/tm-server.log 2>&1 &
echo $! > tm-server.pid
echo "OK"
