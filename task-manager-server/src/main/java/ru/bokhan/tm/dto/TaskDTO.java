package ru.bokhan.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.entity.Task;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "task")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class TaskDTO extends AbstractEntityDTO {

    public static final long serialVersionUID = 1L;

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @NotNull
    @Column(name = "user_id")
    private String userId;

    @NotNull
    @Column(name = "project_id")
    private String projectId;

    public TaskDTO(@NotNull final Task task) {
        this.name = task.getName();
        this.description = task.getDescription();
        this.userId = task.getUser().getId();
        this.setId(task.getId());
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Nullable
    public static TaskDTO toDTO(@Nullable final Task task) {
        if (task == null) return null;
        return new TaskDTO(task);
    }

}
