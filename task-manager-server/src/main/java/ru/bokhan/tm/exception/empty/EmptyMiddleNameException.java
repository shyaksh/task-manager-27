package ru.bokhan.tm.exception.empty;

public final class EmptyMiddleNameException extends RuntimeException {

    public EmptyMiddleNameException() {
        super("Error! Middle Name is empty...");
    }

}