package ru.bokhan.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IServiceLocator;
import ru.bokhan.tm.api.endpoint.IUserEndpoint;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
@AllArgsConstructor
public final class UserEndpoint implements IUserEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().create(login, password);
    }

    @Override
    @WebMethod
    public void createUserWithEmail(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().create(login, password, email);
    }

    @Override
    @WebMethod
    public void registerUserWithEmail(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "email") @Nullable final String email
    ) {
        serviceLocator.getUserService().create(login, password, email);
    }

    @Override
    @WebMethod
    public void createUserWithRole(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password,
            @WebParam(name = "role") @Nullable final Role role
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().create(login, password, role);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO findUserById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findById(id);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO findUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeById(id);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    @WebMethod
    public void updateUser(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName") @Nullable final String middleName,
            @WebParam(name = "email") @Nullable final String email
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService()
                .updateById(session.getUserId(), login, firstName, lastName, middleName, email);
    }

    @Override
    @WebMethod
    public void updateUserById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName") @Nullable final String middleName,
            @WebParam(name = "email") @Nullable final String email
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService()
                .updateById(id, login, firstName, lastName, middleName, email);
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "password") @Nullable final String password
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updatePasswordById(session.getUserId(), password);
    }

    @Override
    @WebMethod
    public void updateUserPasswordById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "password") @Nullable final String password
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().updatePasswordById(id, password);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login") @Nullable final String login
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Override
    @NotNull
    @WebMethod
    public List<UserDTO> findUserAll(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    public void removeUserAll(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().clear();
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "user") @Nullable final UserDTO user
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().remove(user);
    }

}
