package ru.bokhan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void createTask(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "projectId") @Nullable String projectId,
            @WebParam(name = "name") @Nullable String name
    );

    @WebMethod
    void createTaskWithDescription(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "projectId") @Nullable String projectId,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

    @WebMethod
    void addTask(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "task") @Nullable TaskDTO task
    );

    @WebMethod
    void removeTask(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "task") @Nullable TaskDTO task
    );

    @WebMethod
    void removeTaskAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    void removeTaskByUserAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @NotNull
    @WebMethod
    List<TaskDTO> findTaskAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskByIndex(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "index") @Nullable Integer index
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskByName(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "name") @Nullable String name
    );

    @WebMethod
    void removeTaskById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id
    );

    @WebMethod
    void removeTaskByIndex(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "index") @Nullable Integer index
    );

    @WebMethod
    void removeTaskByName(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "name") @Nullable String name
    );

    @WebMethod
    void updateTaskById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

    @WebMethod
    void updateTaskByIndex(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "index") @Nullable Integer index,
            @WebParam(name = "name") @Nullable String name,
            @WebParam(name = "description") @Nullable String description
    );

}
