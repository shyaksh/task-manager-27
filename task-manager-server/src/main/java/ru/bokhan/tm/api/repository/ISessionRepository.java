package ru.bokhan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.api.IRepository;
import ru.bokhan.tm.dto.SessionDTO;

import java.util.List;

public interface ISessionRepository extends IRepository<SessionDTO> {

    List<SessionDTO> findByUserId(@NotNull String userId);

    void removeByUserId(@NotNull String userId);

}