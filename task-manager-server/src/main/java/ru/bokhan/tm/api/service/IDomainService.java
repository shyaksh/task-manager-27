package ru.bokhan.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.dto.Domain;

public interface IDomainService {

    void load(@Nullable Domain domain);

    void save(@Nullable Domain domain);

    boolean saveToXml();

    boolean loadFromXml();

    boolean removeXml();

    boolean saveToJson();

    boolean loadFromJson();

    boolean removeJson();

    boolean saveToBinary();

    boolean loadFromBinary();

    boolean removeBinary();

    boolean saveToBase64();

    @SneakyThrows
    boolean loadFromBase64();

    boolean removeBase64();
}
