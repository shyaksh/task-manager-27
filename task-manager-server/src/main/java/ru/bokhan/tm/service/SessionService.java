package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IServiceLocator;
import ru.bokhan.tm.api.repository.ISessionRepository;
import ru.bokhan.tm.api.service.IPropertyService;
import ru.bokhan.tm.api.service.ISessionService;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.empty.EmptyUserIdException;
import ru.bokhan.tm.exception.security.AccessDeniedException;
import ru.bokhan.tm.repository.SessionRepository;
import ru.bokhan.tm.util.HashUtil;
import ru.bokhan.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionService extends AbstractService<SessionDTO, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }


    @Override
    public void close(@NotNull final SessionDTO session) throws AccessDeniedException {
        validate(session);
        @NotNull final ISessionRepository repository = getRepository();
        @Nullable final SessionDTO found = repository.findById(session.getId());
        remove(found);
    }

    @Override
    public void closeAll(@NotNull final SessionDTO session) throws AccessDeniedException {
        validate(session);
        @NotNull final ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.clear();
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }

    }

    @Nullable
    @Override
    public UserDTO getUser(@NotNull final SessionDTO session) throws AccessDeniedException {
        @NotNull final String userId = getUserId(session);
        @Nullable final UserDTO user = serviceLocator.getUserService().findById(userId);
        return user;
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final SessionDTO session) throws AccessDeniedException {
        validate(session);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new EmptyUserIdException();
        return userId;
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@NotNull final SessionDTO session) throws AccessDeniedException {
        validate(session, Role.ADMIN);
        @NotNull final ISessionRepository repository = getRepository();
        return repository.findAll();
    }

    @Nullable
    @Override
    public SessionDTO sign(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        @NotNull final ISessionRepository repository = getRepository();
        @Nullable final SessionDTO session = repository.findById(sessionDTO.getId());
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean isValid(@NotNull final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final SessionDTO sessionDTO) throws AccessDeniedException {
        if (sessionDTO == null) throw new AccessDeniedException();
        if (sessionDTO.getSignature() == null) throw new AccessDeniedException();
        if (sessionDTO.getUserId() == null) throw new AccessDeniedException();
        if (sessionDTO.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = sessionDTO.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = sessionDTO.getSignature();
        @Nullable final SessionDTO tempSigned = sign(temp);
        if (tempSigned == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = tempSigned.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final ISessionRepository repository = getRepository();
        if (repository.findById(sessionDTO.getId()) == null) throw new AccessDeniedException();
    }

    @Override
    public void validate(@NotNull final SessionDTO session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public SessionDTO open(@NotNull final String login, @NotNull final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @Nullable final UserDTO user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return null;
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.persist(session);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
        return sign(session);
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final UserDTO user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        if (user.getLocked()) return false;
        final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public void signOutByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        @Nullable final UserDTO user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return;
        @NotNull final String userId = user.getId();
        signOutByUserId(userId);
    }

    @Override
    public void signOutByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeByUserId(userId);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    public ISessionRepository getRepository() {
        @NotNull final EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(em);
        return repository;
    }

}
