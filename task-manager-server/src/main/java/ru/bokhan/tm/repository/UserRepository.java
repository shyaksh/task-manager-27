package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.repository.IUserRepository;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<UserDTO> implements IUserRepository {

    public UserRepository(@NotNull EntityManager em) {
        super(em);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        return em.createQuery("SELECT e FROM UserDTO e WHERE e.login = :login", UserDTO.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @Nullable final UserDTO user = findById(id);
        if (user == null) return;
        remove(user);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return;
        remove(user);
    }

    @Override
    @NotNull
    public List<UserDTO> findAll() {
        return em.createQuery("SELECT e FROM UserDTO e", UserDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public UserDTO findById(@NotNull String id) {
        return em.createQuery("SELECT e FROM UserDTO e WHERE e.id = :id", UserDTO.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public long count() {
        return em.createQuery("SELECT COUNT(e) FROM UserDTO e", Long.class).getSingleResult();
    }


    @Override
    public void remove(@NotNull UserDTO userDTO) {
        @Nullable final User user = em.createQuery("SELECT e FROM User e WHERE e.id = :id", User.class)
                .setParameter("id", userDTO.getId())
                .getSingleResult();
        if (user == null) return;
        em.remove(user);
    }

}

