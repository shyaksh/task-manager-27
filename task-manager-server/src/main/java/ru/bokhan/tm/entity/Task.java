package ru.bokhan.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Task extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    @ManyToOne
    private User user;

    @NotNull
    @ManyToOne
    private Project project;

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
