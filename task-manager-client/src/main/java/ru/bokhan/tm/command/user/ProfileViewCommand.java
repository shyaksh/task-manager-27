package ru.bokhan.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.SessionDTO;
import ru.bokhan.tm.endpoint.UserDTO;
import ru.bokhan.tm.exception.security.AccessDeniedException;

public final class ProfileViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "profile-view";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show my profile";
    }


    @Override
    public void execute() {
        @Nullable final SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[SHOW PROFILE]");
        @Nullable String userId = session.getUserId();
        @Nullable final UserDTO user = endpointLocator.getUserEndpoint().findUserById(session, userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
    }

}
